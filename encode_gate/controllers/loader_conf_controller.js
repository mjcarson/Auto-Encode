import Loader_conf from './../models/loader_conf'

const loader_conf_ctrl = {};

// get all loader_confs
loader_conf_ctrl.get_all = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const loader_conf_obj = new Loader_conf();
    Loader_conf.find({}).limit(amt).exec().then((loader_conf_data) => {
        res.status(200).json({
            success: true,
            data: loader_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all loader_confs with name method
loader_conf_ctrl.lname_get = (req, res) => {
    const loader_conf_obj = new Loader_conf();
    Loader_conf.distinct("name").exec().then((loader_conf_data) => {
        res.status(200).json({
            success: true,
            data: loader_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all loader_confs with name method
loader_conf_ctrl.name_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const loader_conf_obj = new Loader_conf();
    Loader_conf.find({name: req.params.name}).limit(amt).exec().then((loader_conf_data) => {
        res.status(200).json({
            success: true,
            data: loader_conf_data[0],
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all loader_confs with id method
loader_conf_ctrl.id_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const loader_conf_obj = new Loader_conf();
    Loader_conf.find({_id: req.params.id}).limit(amt).exec().then((loader_conf_data) => {
        res.status(200).json({
            success: true,
            data: loader_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// post method
loader_conf_ctrl.post = (req, res) => {
    const { name, ip, port, coll_name, nfs_root, storage_root} = req.body;

    // create loader_conf object
    const loader_conf_obj = new Loader_conf({
        name: name,
        ip: ip,
        port: port,
        coll_name: coll_name,
        nfs_root: nfs_root,
        storage_root: storage_root
    });

    loader_conf_obj.save().then((new_loader_conf) => {
        res.status(200).json({
            success: true,
            data: new_loader_conf,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// update method
loader_conf_ctrl.put = (req, res) => {
    var conditions = {};
    conditions[req.params.targ_key] = req.params.targ;
    var updates = req.body;
    var options = {multi: true};

    // update query
    Loader_conf.update(
        conditions,
        updates,
        options
    ).exec().then((new_loader_conf) => {
        res.status(200).json({
            success: true,
            data: new_loader_conf,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

export default loader_conf_ctrl;
