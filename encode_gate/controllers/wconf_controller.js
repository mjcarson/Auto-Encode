import Worker_conf from './../models/worker_conf'

const wconf_ctrl = {};

// get all ld_confs
wconf_ctrl.get_all = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const wconf_obj = new Worker_conf();
    Worker_conf.find({}).limit(amt).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all ld_confs with name method
wconf_ctrl.name_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const wconf_obj = new Worker_conf();
    Worker_conf.find({name: req.params.name}).limit(amt).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};


// get all ld_confs with id method
wconf_ctrl.id_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const wconf_obj = new Worker_conf();
    Worker_conf.find({_id: req.params.id}).limit(amt).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// post method
wconf_ctrl.post = (req, res) => {
    const { name, local_tmp, local_staging, nfs_tmp, nfs_staging, ld_confs} = req.body;

    // create ld_conf object
    const wconf_obj = new Worker_conf({
        name: name,
        local_tmp: local_tmp,
        local_staging: local_staging,
        nfs_tmp: nfs_tmp,
        nfs_staging: nfs_staging,
        ld_confs: ld_confs
    });

    wconf_obj.save().then((new_ld_conf) => {
        res.status(200).json({
            success: true,
            data: new_ld_conf,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// update method
wconf_ctrl.put = (req, res) => {
    var conditions = {};
    conditions[req.params.targ_key] = req.params.targ;
    var updates = req.body;
    var options = {multi: true};

    // update query
    Worker_conf.update(
        conditions,
        updates,
        options
    ).exec().then((new_ld_conf) => {
        res.status(200).json({
            success: true,
            data: new_ld_conf,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

export default wconf_ctrl;
