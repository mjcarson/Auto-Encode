import Job from './../models/jobs'

const job_ctrl = {};

// get all jobs with name method
job_ctrl.name_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const job_obj = new Job();
    Job.find({name: req.params.name, type: req.params.type}).limit(amt).exec().then((job_data) => {
        res.status(200).json({
            success: true,
            data: job_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all jobs with path method
job_ctrl.path_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const job_obj = new Job();
    Job.find({path: Buffer.from(req.params.path, 'base64').toString('ascii'), type: req.params.type}).limit(amt).exec().then((job_data) => {
        res.status(200).json({
            success: true,
            data: job_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all jobs with assigned status type method
job_ctrl.status_type_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const job_obj = new Job();
    Job.find({status: req.params.status, type: req.params.type}).limit(amt).exec().then((job_data) => {
        res.status(200).json({
            success: true,
            data: job_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all jobs with assigned status method
job_ctrl.status_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const job_obj = new Job();
    Job.find({status: req.params.status}).limit(amt).exec().then((job_data) => {
        res.status(200).json({
            success: true,
            data: job_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all jobs with id method
job_ctrl.id_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const job_obj = new Job();
    Job.find({_id: req.params.id}).limit(amt).exec().then((job_data) => {
        res.status(200).json({
            success: true,
            data: job_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get job without type selection
job_ctrl.get_job = (req, res) => {
    // get job
    const job_obj = new Job();
    Job.findOneAndUpdate({status:req.params.ostatus}, {$set:{status:req.params.nstatus}}, {projection: {"_id": 0, "__v": 0}, new: true}, (err, doc) => {
      if (err) {
        res.status(500).json({
            success: false,
            message: err,
        }); 
      }
      else if(doc != null){
        console.log(doc["_doc"]);
        res.status(200).json({
            success: true,
            data: doc["_doc"],
        });
      } else {
        res.status(200).json({
            success: true,
            data: null,
        });
      }
    });
};

// get new job for worker with job type selection
job_ctrl.get_job_type = (req, res) => {
    // get job
    const job_obj = new Job();
    Job.findOneAndUpdate({type: req.params.type, status:req.params.ostatus}, {$set:{status:req.params.nstatus}}, {projection: {"_id": 0, "__v": 0}, new: true}, (err, doc) => {
      if (err) {
        res.status(500).json({
            success: false,
            message: err,
        }); 
      }
      else if(doc != null){
        console.log(doc["_doc"]);
        res.status(200).json({
            success: true,
            data: doc["_doc"],
        });
      } else {
        res.status(200).json({
            success: true,
            data: null,
        });
      }
    });
};

// post method
job_ctrl.post = (req, res) => {
    const { name, path, type, total_time, presets, targ_path, staging_path, ld_tmp_root, ld_staging_root, ld_store_root} = req.body;

    // create job object
    const job_obj = new Job({
        name: name,
        path: path,
        type: type,
        total_time: total_time,
        presets: presets,
        targ_path: targ_path,
        staging_path: staging_path,
        ld_tmp_root: ld_tmp_root,
        ld_staging_root: ld_staging_root,
        ld_store_root: ld_store_root
    });

    job_obj.save().then((new_job) => {
        res.status(200).json({
            success: true,
            data: new_job,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// update method
job_ctrl.put = (req, res) => {
    var conditions = {};
    conditions[req.params.targ_key] = req.params.targ;
    var updates = req.body;
    var options = {multi: true};

    // update query
    Job.update(
        conditions,
        updates,
        options
    ).exec().then((new_job) => {
        res.status(200).json({
            success: true,
            data: new_job,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

export default job_ctrl;
