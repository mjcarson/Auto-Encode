import Ld_conf from './../models/ld_conf'

const ldconf_ctrl = {};

// get all ld_confs
ldconf_ctrl.get_all = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const ld_conf_obj = new Ld_conf();
    Ld_conf.find({}).limit(amt).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all ld_confs with name method
ldconf_ctrl.name_get = (req, res) => {
    const ld_conf_obj = new Ld_conf();
    Ld_conf.findOne({name: req.params.name}).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all ld_confs with lconfname method
ldconf_ctrl.lconf_name_get = (req, res) => {
    const ld_conf_obj = new Ld_conf();
    Ld_conf.find({lconf_name: req.params.lconf_name}).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all ld_confs with id method
ldconf_ctrl.id_get = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const ld_conf_obj = new Ld_conf();
    Ld_conf.find({_id: req.params.id}).limit(amt).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// get all unique names of ldconfs
ldconf_ctrl.ldname_get = (req, res) => {
    const ld_conf_obj = new Ld_conf();
    Ld_conf.distinct("name").exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// post method
ldconf_ctrl.post = (req, res) => {
    const { name, lconf_name, load_dir, targ_dir, nfs_root, presets, types, staging} = req.body;

    // create ld_conf object
    const ld_conf_obj = new Ld_conf({
        name: name,
        lconf_name: lconf_name,
        load_dir: load_dir,
        targ_dir: targ_dir,
        nfs_root: nfs_root,
        presets: presets,
        types: types,
        staging: staging
    });

    ld_conf_obj.save().then((new_ld_conf) => {
        res.status(200).json({
            success: true,
            data: new_ld_conf,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// update method
ldconf_ctrl.put = (req, res) => {
    var conditions = {};
    conditions[req.params.targ_key] = req.params.targ;
    var updates = req.body;
    var options = {multi: true};

    // update query
    Ld_conf.update(
        conditions,
        updates,
        options
    ).exec().then((new_ld_conf) => {
        res.status(200).json({
            success: true,
            data: new_ld_conf,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

export default ldconf_ctrl;
