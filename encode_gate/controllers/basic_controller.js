const basic_ctrl = {};

// default get response
basic_ctrl.get = (req, res) => {
    console.log(req);
    res.json({
        message: 'Auto-Encode Cluster API'
    });
};

export default basic_ctrl;
