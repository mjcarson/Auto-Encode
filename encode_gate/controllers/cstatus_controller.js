import Cstatus from './../models/cstatus'

const cstatus_ctrl = {};

// get all ld_confs
cstatus_ctrl.get_all = (req, res) => {
    var amt = parseInt(req.params.amt);
    // set amt to 1 if its not passed
    if (!amt) {
        amt = 1;
    }

    const cstatus_obj = new Cstatus();
    Cstatus.find({}).limit(amt).exec().then((ld_conf_data) => {
        res.status(200).json({
            success: true,
            data: ld_conf_data,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// post method
cstatus_ctrl.post = (req, res) => {
    const {cstatus} = req.body;
    // build query 
    Cstatus.findOneAndUpdate({'name':'cluster'}, {'cstatus': 'alive'}, {upsert:true}).exec().then((new_cstatus) => {
        res.status(200).json({
            success: true,
            data: new_cstatus,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

// update method
cstatus_ctrl.put = (req, res) => {
    var conditions = {};
    conditions[req.params.targ_key] = req.params.targ;
    var updates = req.body;
    var options = {multi: true};

    // update query
    Cstatus.update(
        conditions,
        updates,
        options
    ).exec().then((new_cstatus) => {
        res.status(200).json({
            success: true,
            data: new_cstatus,
        });
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            success: false,
            message: err,
        });
    });
};

export default cstatus_ctrl;
