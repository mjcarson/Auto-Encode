import app from './server';

app.listen(process.env.API_PORT, () => {
    console.log('[*] Running on port ' + process.env.API_PORT);
});
