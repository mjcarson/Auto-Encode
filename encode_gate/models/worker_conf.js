import mongoose from 'mongoose';
require('mongoose-long')(mongoose)

// setup schema data type
const { Schema } = mongoose;

// set promise library
mongoose.Promise = global.Promise;

// mongoose datatypes
var schema_types = mongoose.Schema.Types;
var num_long = mongoose.Types.Long;

// make schema
const worker_conf_schema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [1, "name must be 1 characters or more."],
    },
    local_tmp: {
        type: String,
        required: true,
        minlength: [1, "local_tmp must be 1 characters or more."],
    },
    local_staging: {
        type: String,
        required: true,
        minlength: [1, "local_staging must be 1 characters or more."],
    },
    nfs_tmp: {
        type: String,
        required: true,
        minlength: [1, "nfs_tmp must be 1 characters or more."],
    },
    nfs_staging: {
        type: String,
        required: true,
        minlength: [1, "nfs_staging must be 1 characters or more."],
    },
    ld_confs: {
        type: String,
        required: true,
        minlength: [1, "ld_confs must be 1 characters or more."],
    },
});

const Worker_conf = mongoose.model('worker_conf', worker_conf_schema);
export default Worker_conf;
