import mongoose from 'mongoose';
require('mongoose-long')(mongoose)

// uuiv4
const uuidv4 = require('uuid/v4');

// setup schema data type
const { Schema } = mongoose;

// set promise library
mongoose.Promise = global.Promise;

// mongoose datatypes
var schema_types = mongoose.Schema.Types;
var num_long = mongoose.Types.Long;

// make schema
const job_schema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [1, "name must be 1 characters or more."],
    },
    path: {
        type: String,
        required: true,
        minlength: [1, "path must be 1 characters or more."],
    },
    targ_path: {
        type: String,
        required: true,
        minlength: [1, "targ_path must be 1 characters or more."],
    },
    staging_path: {
        type: String,
        required: true,
        minlength: [1, "staging_path must be 1 characters or more."],
    },
    job_id: {
        type: String,
        required: true,
        default: uuidv4
    },
    status: {
        // possible values for status are
        // recieved - job is recieved but not assigned to a worker
        // assigned - job has been assigned to a worker
        // downloading - file is being copied to worker
        // converting - conversion process has been started
        // uploading - file is being uploaded to storage server
        // insertion - file is being copied from temp to final destination
        // done - work on file is complete
        type: String,
        required: false,
        minlength: [1, "status name must be 1 characters or more."],
        default: "recieved",
    },
    dlprog: {
        type: schema_types.Long,
        required: true,
        default: num_long.fromString("0"),
    },
    prog: {
        type: schema_types.Long,
        required: true,
        default: num_long.fromString("0"),
    },
    time: {
        type: schema_types.Long,
        required: true,
        default: num_long.fromString("0"),
    },
    total_time: {
        type: schema_types.Long,
        required: true,
    },
    type: {
        type: String,
        required: true,
        minlength: [1, "type must be 1 characters or more."],
    },
    presets: {
        type: String,
        required: false,
        minlength: [1, "preset must be 1 characters or more."],
    },
    worker: {
        type: String,
        required: false,
        minlength: [1, "worker must be 1 characters or more."],
        default: "unassigned",
    },
    ld_tmp_root: {
        type: String,
        required: true,
        minlength: [1, "ld_tmp_root must be 1 characters or more."],
    },
    ld_staging_root: {
        type: String,
        required: true,
        minlength: [1, "ld_staging_root must be 1 characters or more."],
    },
    ld_store_root: {
        type: String,
        required: true,
        minlength: [1, "ld_store_root must be 1 characters or more."],
    },
});

const Job = mongoose.model('Jobs', job_schema);
export default Job;
