import mongoose from 'mongoose';
require('mongoose-long')(mongoose)

// setup schema data type
const { Schema } = mongoose;

// set promise library
mongoose.Promise = global.Promise;

// mongoose datatypes
var schema_types = mongoose.Schema.Types;
var num_long = mongoose.Types.Long;

// make schema
const loader_conf_schema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [1, "name must be 1 characters or more."],
    },
    ip: {
        type: String,
        required: true,
        minlength: [1, "ip must be 1 characters or more."],
    },
    port: {
        type: Number,
        required: true,
        minlength: [1, "port must be 1 characters or more."],
    },
    coll_name: {
        type: String,
        required: true,
        minlength: [1, "coll_name must be 1 characters or more."],
    },
    nfs_root: {
        type: String,
        required: true,
        minlength: [1, "nfs_root must be 1 characters or more."],
    },
    storage_root: {
        type: String,
        required: true,
        minlength: [1, "storage_root must be 1 characters or more."],
    },
});

const Loader_conf = mongoose.model('loader_conf', loader_conf_schema);
export default Loader_conf;
