import mongoose from 'mongoose';
require('mongoose-long')(mongoose)

// setup schema data type
const { Schema } = mongoose;

// set promise library
mongoose.Promise = global.Promise;

// mongoose datatypes
var schema_types = mongoose.Schema.Types;
var num_long = mongoose.Types.Long;

// make schema
const ld_conf_schema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [1, "name must be 1 characters or more."],
    },
    lconf_name: {
        type: String,
        required: true,
        minlength: [1, "lconf_name must be 1 characters or more."],
    },
    load_dir: {
        type: String,
        required: true,
        minlength: [1, "load_dir must be 1 characters or more."],
    },
    targ_dir: {
        type: String,
        required: true,
        minlength: [1, "targ_dir must be 1 characters or more."],
    },
    nfs_root: {
        type: String,
        required: true,
        minlength: [1, "nfs_root must be 1 characters or more."],
    },
    presets: {
        type: String,
        required: true,
        minlength: [1, "presets must be 1 characters or more."],
    },
    types: {
        type: String,
        required: true,
        minlength: [1, "types must be 1 characters or more."],
    },
    staging: {
        type: String,
        required: true,
        minlength: [1, "staging must be 1 characters or more."],
    },
    contaier: {
        type: String,
        required: false,
        default: "mkv",
        minlength: [1, "container must be 1 characters or more."],
    },
});

const Ld_conf = mongoose.model('ld_conf', ld_conf_schema);
export default Ld_conf;
