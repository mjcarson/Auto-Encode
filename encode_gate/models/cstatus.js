import mongoose from 'mongoose';
require('mongoose-long')(mongoose)

// setup schema data type
const { Schema } = mongoose;

// set promise library
mongoose.Promise = global.Promise;

// mongoose datatypes
var schema_types = mongoose.Schema.Types;
var num_long = mongoose.Types.Long;

// make schema
const cstatus_schema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: [1, "status must be 1 characters or more."],
    },
    cstatus: {
        type: String,
        required: true,
        minlength: [1, "status must be 1 characters or more."],
    },
});

const Cstatus_schema = mongoose.model('cstatus', cstatus_schema);
export default Cstatus_schema;
