import express from 'express';

// controller imports
import basic_ctrl from './controllers/basic_controller';
import job_ctrl from './controllers/job_controller';
import loader_conf_ctrl from './controllers/loader_conf_controller';
import ldconf_ctrl from './controllers/ldconf_controller';
import wconf_ctrl from './controllers/wconf_controller';
import cstatus_ctrl from './controllers/cstatus_controller';

const routes = express.Router();

// basic routes
routes.get('/', basic_ctrl.get);

// job routes
routes.get('/jobs/name/:type/:name/:amt?/', job_ctrl.name_get);
routes.get('/jobs/path/:type/:path/:amt?/', job_ctrl.path_get);
routes.get('/jobs/status_type/:type/:status/:amt?/', job_ctrl.status_type_get);
routes.get('/jobs/status/:status/:amt?/', job_ctrl.status_get);
routes.get('/jobs/id/:id/:amt?/', job_ctrl.id_get);
routes.get('/jobs/get_job/:ostatus/:nstatus/', job_ctrl.get_job);
routes.get('/jobs/get_job/:type/:ostatus/:nstatus/', job_ctrl.get_job_type);
routes.post('/jobs/', job_ctrl.post);
routes.put('/jobs/:targ_key/:targ/', job_ctrl.put);

// loader_conf routes
routes.get('/loaderconfs/getall/:amt?/', loader_conf_ctrl.get_all);
routes.get('/loaderconfs/name/:name/:amt?/', loader_conf_ctrl.name_get);
routes.get('/loaderconfs/id/:id/:amt?/', loader_conf_ctrl.id_get);
routes.get('/loaderconfs/lnames/', loader_conf_ctrl.lname_get);
routes.post('/loaderconfs/', loader_conf_ctrl.post);
routes.put('/loaderconfs/:targ_key/:targ/', loader_conf_ctrl.put);

// ld_conf routes
routes.get('/ldconfs/getall/:amt?/', ldconf_ctrl.get_all);
routes.get('/ldconfs/name/:name/', ldconf_ctrl.name_get);
routes.get('/ldconfs/lconfname/:lconf_name/', ldconf_ctrl.lconf_name_get);
routes.get('/ldconfs/ldnames/', ldconf_ctrl.ldname_get);
routes.get('/ldconfs/id/:id/:amt?/', ldconf_ctrl.id_get);
routes.post('/ldconfs/', ldconf_ctrl.post);
routes.put('/ldconfs/:targ_key/:targ/', ldconf_ctrl.put);

// worker_conf routes
routes.get('/workerconfs/getall/:amt?/', wconf_ctrl.get_all);
routes.get('/workerconfs/name/:name/:amt?/', wconf_ctrl.name_get);
routes.get('/workerconfs/id/:id/:amt?/', wconf_ctrl.id_get);
routes.post('/workerconfs/', wconf_ctrl.post);
routes.put('/workerconfs/:targ_key/:targ/', wconf_ctrl.put);

// cstatus routes
routes.get('/cstatus/getall/:amt?/', cstatus_ctrl.get_all);
routes.post('/cstatus/', cstatus_ctrl.post);
routes.put('/cstatus/:targ_key/:targ/', cstatus_ctrl.put);

export default routes;
