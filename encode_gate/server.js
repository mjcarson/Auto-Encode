import express from 'express';
import mongoose from 'mongoose';
import body_parser from 'body-parser';
require('dotenv').config();

import routes from './routes';

mongoose.connect('mongodb://' + process.env.MONGO_ADDR + '/encode', {useMongoClient: true}, () => {
    console.log('[*] Connected to mongodb...');
});

const app = express();

//Middleware
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended:true}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/encode', routes);

export default app;
