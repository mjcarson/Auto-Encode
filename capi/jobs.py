#!/usr/bin/env python3
'''jobs funcs used by capi'''

import json
import socket
import requests


class Jobs(object):
    '''methods for talking to the Jobs Auto-Encode routes'''
    
    class InvalidJob(Exception):
        '''Job data is missing a required field'''
        pass

    def add_job(self, jdata):
        '''
        Adds job to Auto-Encode

        Args:
            jdata (dict): dictionary containing all job data
        '''
        # Check all required job data is present
        required = ["name", \
                    "path", \
                    "type", \
                    "total_time", \
                    "presets", \
                    "targ_path", \
                    "staging_path", \
                    "ld_tmp_root", \
                    "ld_staging_root", \
                    "ld_store_root"]
        for item in required:
            if item not in jdata:
                self.log.error("{} is a required field".format(item))
                raise self.InvalidJob
        
        # build url
        targ = "{}/jobs".format(self.api_root)

        # sumbit job to Auto-Encode
        try:
            req = requests.post(targ, \
                                data=jdata, \
                                verify=self.config["ssl_verify"])
            return req
        except socket.error:
            self.log.error("{} connection refused".format(targ))


    def get_job(self, ostatus="recieved", nstatus="assigned", jtype=None):
        '''
        gets a job of a certain type

        Args:
            jtype (string): type of job to retrieve
        Returns:
            dictionary: job data
        '''
        # build url based on jtpe presence
        if jtype is None:
            # get job based solely on status
            targ = "{}/jobs/get_job/{}/{}/".format(self.api_root, \
                                                  ostatus, \
                                                  nstatus)
        else:
            # get job based on type & status
            targ = "{}/jobs/get_job/{}/{}/{}/".format(self.api_root, \
                                                  jtype, \
                                                  ostatus, \
                                                  nstatus)

        # get job data
        try:
            req = requests.get(targ, verify=self.config["ssl_verify"])
            job_data = json.loads(req.text)
            if job_data["data"] is not None:
                return job_data["data"]
        except socket.error:
            self.log.error("{} connection refused".format(targ))

    def set_job_status(self, job_id, status):
        '''
        sets job status
        
        Args:
            job_id (string): job ID
            status (string): new status to set for job
        '''
        # build url
        targ = "{}/jobs/job_id/{}/".format(self.api_root, job_id)

        # build data
        data = {}
        data["status"] = status

        # set job's status
        try:
            req = requests.put(targ, \
                               data=data, \
                               verify=self.config["ssl_verify"])
            return req
        except socket.error:
            self.log.error("{} connection refused".format(targ))
    
    def set_job_worker(self, job_id, worker):
        '''
        sets worker for a job by job_id

        Args:
            job_id (string): job ID
            Worker (string): name of worker (usually hostname)
        '''
        # build url
        targ = "{}/jobs/job_id/{}/".format(self.api_root, job_id)

        # build data
        data = {}
        data["worker"] = worker

        # set job's worker
        try:
            req = requests.put(targ, \
                               data=data, \
                               verify=self.config["ssl_verify"])
            return req
        except socket.error:
            self.log.error("{} connection refused".format(targ))


    def set_job_transcoded_time(self, job_id, time):
        '''
        sets transcoded time for a job by job_id

        Args:
            job_id (string): job ID
            time (string, int): current position of transcode in the media
        '''
        # build url
        targ = "{}/jobs/job_id/{}/".format(self.api_root, job_id)

        # build data
        data = {}
        data["time"] = time

        # set job's transcoded time
        try:
            req = requests.put(targ, \
                               data=data, \
                               verify=self.config["ssl_verify"])
            return req
        except socket.error:
            self.log.error("{} connection refused".format(targ))

    def set_job_progress(self, job_id, progress):
        '''
        sets current progress for a job by job_id

        Args:
            job_id (string): job ID
            progress (string): curent progress of job
        '''
        # build url
        targ = "{}/jobs/job_id/{}/".format(self.api_root, job_id)

        # build data
        data = {}
        data["prog"] = progress

        # set job's progress
        try:
            req = requests.put(targ, \
                               data=data, \
                               verify=self.config["ssl_verify"])
            return req
        except socket.error:
            self.log.error("{} connection refused".format(targ))

    def set_job_dl_progress(self, job_id, dlprogress):
        '''
        sets current download progress for a job by job_id

        Args:
            job_id (string): job ID
            dlprogress (string): current amount of file downloaded
        '''
        # build url
        targ = "{}/jobs/job_id/{}/".format(self.api_root, job_id)

        # build data
        data = {}
        data["dlprog"] = dlprogress

        # set job's download progress
        try:
            req = requests.put(targ, \
                               data=data, \
                               verify=self.config["ssl_verify"])
            return req
        except socket.error:
            self.log.error("{} connection refused".format(targ))

    def get_job_path(self, ldname, path):
        '''
        gets a job by its path

        Args:
            ldname (string): name of load directory config
            path (string): job path
        '''
        # build url
        targ = "{}/jobs/path/{}/{}/".format(self.api_root, ldname, path)

        # get jobs with path
        try:
            req = requests.get(targ, \
                               verify=self.config["ssl_verify"])
            # return job data if it exists 
            job_data = json.loads(req.text)
            if job_data["data"] is not None:
                return job_data["data"]
        except socket.error:
            self.log.error("{} connection refused".format(targ))
        except json.decoder.JSONDecodeError:
            self.log.error("{} failed to decode JSON".format(targ), exc_info=True)

