#!/usr/bin/env python3
'''cstatus funcs used by capi'''

import json
import socket
import requests

class Cstatus(object):
    '''methods for talking to the Cstatus Auto-Encode routes'''

    def get_cstatus(self):
        '''gets cluster status'''
        # build target url
        targ = "{}/cstatus/getall/".format(self.api_root)
        
        # get cstatus
        try:
            req = requests.get(targ, verify=self.config["ssl_verify"])
            return json.loads(req.text)
        except socket.error:
            self.log.error("{} connection refused".format(targ))

    def check_cstatus(self):
        '''
        check if cluster is shutting down
        
        Returns:
            boolean: True if shutting down or False if not
        '''
        # get cstatus
        cstatus = self.get_cstatus()

        # check status
        if cstatus is not None:
            if cstatus["data"][0]["cstatus"] == "shutdown":
                return True
        return False

    def init_cstatus(self):
        '''initalizes cluster status'''
        # build target url
        targ = "{}/cstatus/".format(self.api_root)

        # init cstatus
        try:
            req = requests.post(targ, verify=self.config["ssl_verify"])
        except socket.error:
            self.log.error("{} connection refused".format(targ))

