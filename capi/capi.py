#!/usr/bin/env python3
'''API client for Auto-Encode API'''

import os
import yaml
import logging
import logging.handlers

# capi route group imports
from .jobs import Jobs
from .cstatus import Cstatus
from .ldconfs import Ldconfs
from .workerconfs import Wconfs
from .loaderconfs import Loader_confs

class Capi(Jobs, Cstatus, Ldconfs, Wconfs, Loader_confs):
    '''Client for talking to Auto-Encode API'''
    def __init__(self, cfg_path="../confs/ae.yaml", log=None):
        '''inits capi'''
        # load yaml
        #with open("/Auto-Encode/confs/ae.yaml") as fp:
        with open(cfg_path) as fp:
            self.config = yaml.load(fp, Loader=yaml.FullLoader)
            # extract capi config
            self.config = self.config["capi"]
       
        # setup logging
        if log is not None:
            self.log = log
        else:
            # ensure log path exists
            parent = os.path.dirname(self.config["log"]["path"])
            os.makedirs(parent, exist_ok=True)

            # setup logging
            self.log = logging.getLogger(__name__)
            self.log.setLevel(logging.DEBUG)

            # create console handler
            console = logging.StreamHandler()
            console.setLevel(logging.DEBUG)

            # create a file handler
            fhandler = logging.handlers.TimedRotatingFileHandler(\
                            self.config["log"]["path"], \
                            when='D', \
                            interval=1, \
                            backupCount=3)
            fhandler.setLevel(self.config["log"]["level"].upper())

            # create a logging format
            formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
            fhandler.setFormatter(formatter)
            console.setFormatter(formatter)

            # add the handlers to the logger
            self.log.addHandler(fhandler)
            # use console_log
            if self.config["log"]["console"] == True:
                self.log.addHandler(console)

        # set ssl
        if self.config["ssl"] == True:
            protocol = "https"
            # disable insure warning
            if self.config["ssl_verify"] == False:
                urllib3.disable_warnings()
        else:
            protocol = "http"

        # api targ
        self.api_root = protocol + "://" + self.config["api_url"] + "/" + \
                self.config["endpoint"]

if __name__ == "__main__":
    capi = Capi()
    print(capi.get_worker_conf("Default"))
