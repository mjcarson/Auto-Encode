'''loader_confs funcs used by capi'''

import json
import socket
import requests

class Loader_confs(object):
    '''methods for talking to the loader_confs Auto-Encode routes'''

    def get_loader_conf(self, name):
        '''
        gets a loader config by name

        Args:
            name (string): name of loader config to retrieve
        Returns:
            dictionary: loader_conf
        '''
        # build url
        targ = "{}/loaderconfs/name/{}/".format(self.api_root, name)

        # get loader config
        try:
            req = requests.get(targ, verify=self.config["ssl_verify"])
            lconf = json.loads(req.text)
            if lconf["data"] is not None:
                return lconf["data"]
        except socket.error:
            self.log.error("{} connection refused".format(targ))

