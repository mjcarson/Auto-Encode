'''load directory confs funcs used by capi'''

import json
import socket
import requests

class Ldconfs(object):
    '''methods for talking to the ldconfs Auto-Encode routes'''

    def get_ldconf(self, name):
        '''
        gets a load directory config by name

        Args:
            name (string): name of load directory config to retrieve
        Returns:
            dictionary: load directory config
        '''
        # build url
        targ = "{}/ldconfs/name/{}/".format(self.api_root, name)

        # get loader config
        try:
            req = requests.get(targ, verify=self.config["ssl_verify"])
            ldconf = json.loads(req.text)
            if ldconf["data"] is not None:
                return ldconf["data"]
        except socket.error:
            self.log.error("{} connection refused".format(targ))

    def get_ldconfs_loader(self, name):
        '''
        gets all load directory confs for a particular loader config

        Args:
            name (string): name of loader config to retrieve ldconfs for
        Returns:
            [dictionary]: list of load directory confs
        '''
        # build url
        targ = "{}/ldconfs/lconfname/{}/".format(self.api_root, name)

        # get loader config
        try:
            req = requests.get(targ, verify=self.config["ssl_verify"])
            ldconfs = json.loads(req.text)
            if ldconfs["data"] is not None:
                return ldconfs["data"]
        except socket.error:
            self.log.error("{} connection refused".format(targ))

