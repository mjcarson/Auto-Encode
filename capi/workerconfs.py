'''worker config funcs used by capi'''

import json
import socket
import requests

class Wconfs(object):
    '''methods for talking to the workerconfs Auto-Encode routes'''

    def get_worker_conf(self, name):
        '''
        gets a worker config by name

        Args:
            name (string): name of worker config to retrieve
        Returns:
            dictionary: worker config
        '''
        # build url
        targ = "{}/workerconfs/name/{}/".format(self.api_root, name)

        # get worker config
        try:
            req = requests.get(targ, verify=self.config["ssl_verify"])
            wconf = json.loads(req.text)
            if wconf["data"] is not None:
                return wconf["data"]
        except socket.error:
            self.log.error("{} connection refused".format(targ))

