#!/usr/bin/env python3
'''Loads files into Auto-Encode and inserts them into their final location'''

import os
import json
import time
import yaml
import logging
import argparse
import logging.handlers
import platform

from threading import Thread, Lock

# AE api client
import capi.capi as capi

# AE utlities
import utils.ldir as ldir 
import utils.utils as utils

class Loader(object):
    '''Loader for Auto-Encode'''
    def __init__(self, cfg_path="confs/ae.yaml", log=None):
        '''
        inits Loader
        
        Args:
            cfg_path (string): path to config file
            log (logging object): logging object
        '''
        # load yaml
        #with open("/Auto-Encode/confs/ae.yaml") as fp:
        with open(cfg_path) as fp:
            self.fconfig = yaml.load(fp, Loader=yaml.FullLoader)
            # extract capi config
            self.config = self.fconfig["loader"]
       
        # setup logging
        if log is not None:
            self.log = log
        else:
            # ensure log path exists
            parent = os.path.dirname(self.config["log"]["path"])
            os.makedirs(parent, exist_ok=True)

            # setup logging
            self.log = logging.getLogger(platform.node())
            self.log.setLevel(logging.DEBUG)

            # create console handler
            console = logging.StreamHandler()
            console.setLevel(logging.DEBUG)

            # create a file handler
            fhandler = logging.handlers.TimedRotatingFileHandler(\
                            self.config["log"]["path"], \
                            when='D', \
                            interval=1, \
                            backupCount=3)
            fhandler.setLevel(self.config["log"]["level"].upper())

            # create a logging format
            formatter = logging.Formatter('%(asctime)s [%(name)s] [%(processName)s] [%(levelname)s] %(message)s')
            fhandler.setFormatter(formatter)
            console.setFormatter(formatter)

            # add the handlers to the logger
            self.log.addHandler(fhandler)
            # use console_log
            if self.config["log"]["console"] == True:
                self.log.addHandler(console)

        # api client
        self.capi = capi.Capi(cfg_path, self.log)
        
        # ae utilities
        self.utils = utils.Utils(self.config, self.log, self.capi)

        # setup vars
        self.threads = []
        self.ldirs = []

        # log spam protection
        self.spam = []
    
    def _setup(self):
        '''
        sets loader up and allows for the loader config to be updated 
        dynamically
        '''
        # clear old load dirs
        # this is to allow load dirs to be updated without restarting loader
        del self.ldirs[:]
        # setup load dirs
        for wdir in self.fconfig["watch_dirs"]:
            if wdir["loader"] == self.config["name"]:
                # build load dir object
                dir_obj = ldir.Load_dir(wdir["name"], \
                                   wdir["load_dir"], \
                                   wdir["targ_dir"], \
                                   wdir["share_root"], \
                                   wdir["presets"], \
                                   wdir["type"], \
                                   wdir["staging_root"], \
                                   wdir["container"])
                self.ldirs.append(dir_obj)


    def _insertion_handler(self):
        '''inserts finished conversions to their final destination'''
        while 1:
            # check if cluster is shutting down
            if self.capi.get_cstatus() == "shutdown":
                self.log.info("Shutting down")
                break

            # get insertion job
            jdata = self.capi.get_job("uploaded", "insertion")
            if jdata is not None:
                # insert job found
                self.log.info("Inserting {}".format(jdata["name"]))
                # build paths
                staging_path = os.path.join(jdata["ld_staging_root"], jdata["staging_path"])
                targ_path = os.path.join(jdata["ld_store_root"], jdata["targ_path"])
                # copy to final destination
                istatus = self.utils.copy(staging_path, targ_path, jdata["job_id"], False)
                if istatus is False:
                    self.capi.set_job_status(jdata["job_id"], "uploaded")
                    continue
                self.capi.set_job_status(jdata["job_id"], "done")
                self.log.info("Insertion complete  {}".format(jdata["name"]))
                # build paths to remove
                tmp_path = os.path.join(jdata["ld_tmp_root"], jdata["path"])
                time.sleep(20)
                # remove old files
                os.remove(staging_path)
                os.remove(tmp_path)
                time.sleep(5)
            else:
                time.sleep(5)
    
    def _load_media(self, ldir, paths, mtype):
        '''
        loads media into Auto-Encode

        Args:
            ldir (ldir.Load_dir): load directory object
            paths (tuple): os.walk tuple with media found
            mtype (string): type of media to load (movie/tv)
        '''
        # build subpath
        if mtype.lower() in ["movie", "movie_itunes"]:
            # build subpath for movie
            subpath = paths[0].split("/")
            subpath = os.path.join(*subpath[-1:])
        if mtype.lower() in ["tv", "tv_itunes"]:
            # build subpath for tv
            subpath = paths[0].split("/")
            subpath = os.path.join(*subpath[-2:])
        # build job template
        jtemp = {}
        jtemp["path"] = os.path.join(ldir.type, subpath)
        jtemp["type"] = ldir.name
        jtemp["presets"] = ldir.presets
        jtemp["targ_path"] = subpath
        jtemp["ld_tmp_root"] = os.path.join(ldir.share_root, "tmp")
        jtemp["ld_staging_root"] = ldir.staging_path
        jtemp["ld_store_root"] = ldir.targ_path
        # load all media files
        for media in paths[2]:
            # skip temp/hidden files starting with .
            if media[0] == ".":
                continue

            # copy template and build job data
            jdata = dict(jtemp)
            # build path
            jdata["path"] = os.path.join(jdata["path"], media)
            
            # duplicate checks
            if self.config["duplicate_check"] != False:
                # check if duplicate job
                if self.utils.check_dup_job(ldir.name, jdata["path"]) == True:
                    if jdata["path"] not in self.spam:
                        self.log.info("{} has already been encoded skipping".format(media))
                        self.spam.append(jdata["path"])
                    continue
            
            # remove from spam list
            if jdata["path"] in self.spam:
                self.spam.remove(jdata["path"])

            # build src & nfs paths
            src_path = os.path.join(ldir.load_path, subpath, media)
            nfs_path = os.path.join(ldir.share_root, "tmp", jtemp["path"], media)
            self.log.debug("src path: {}".format(src_path))
            self.log.debug("nfs path: {}".format(nfs_path))
            # copy file to tmp dir if its different then watch dir
            if src_path != nfs_path:
                self.utils.copy(src_path, nfs_path)

            # get name with the new extension
            ncname = self.utils.get_ncname(media, ldir)
            jdata["name"] = media
            jdata["targ_path"] = os.path.join(subpath, ncname)
            jdata["staging_path"] = ncname

            # get total time of media
            jdata["total_time"] = self.utils.get_runtime(nfs_path)

            # add job to capi
            if self.capi.add_job(jdata).status_code == 200:
                os.remove(src_path)

    def _monitor(self, ldir):
        '''
        monitors a directory for new files to encode

        Args:
            ldir (ldir.Load_dir): load directory object
        '''
        # make sure have a valid path
        if os.path.isdir(ldir.load_path) == False:
            self.log.error("{}:{} is not a valid path".format(ldir.name, \
                                                              ldir.load_path))
        else:
            if ldir.name + ldir.type not in self.spam:
                self.log.info("{} monitor started as type {}".format(ldir.name, ldir.type))
                self.spam.append(ldir.name + ldir.type)
            # get all media to encode
            for media in self.utils.filter(ldir.load_path, 2):
                # load all media
                self._load_media(ldir, media, ldir.type)

    def start(self):
        '''Start loader'''
        self.log.info("Loader started")

        # spawn insertion handler
        t = Thread(target=self._insertion_handler)
        self.threads.append(t)
        t.start()

        # monitor thread dict
        monitors = {}
        # enter main work loop
        while 1:
            # check if cluster is shutting down
            if self.capi.get_cstatus() == "shutdown":
                self.log.info("Shutting down")
                break

            # refresh loader config
            self._setup()

            # spawn monitors
            for ldir in self.ldirs:
                # only spawn if monitor if its not alive
                if ldir.name not in monitors.keys():
                    self._monitor(ldir)

            # prune dead monitor threads
            self.utils.cull_workers(monitors)
            time.sleep(30)
        
        # wait for threads to exit before exiting
        while 1:
            # remove dead workers
            self.utils.cull_workers(monitors)

            # break if workers is empty
            if len(workers.keys()) == 0:
                break

        
        # wait for all threads in self.threads to exit
        for t in self.threads:
            if t.isAlive():
                t.join()

def argparser():
    '''parses arguments for loader'''
    parser = argparse.ArgumentParser(description=\
                                     "Loads files into Auto-Encode")
    parser.add_argument('--conf', help="path to config file")
    return parser.parse_args()

if __name__ == "__main__":
    # parse args
    args = argparser()
    ldr = Loader(args.conf)
    ldr.start()
