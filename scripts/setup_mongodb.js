// connect to mongodb
conn = new Mongo();
db = conn.getDB("encode");
// make index forcing unique loader_conf names
db.loader_confs.createIndex({"name": 1}, {unique: true})
