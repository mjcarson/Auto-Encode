#!/bin/bash

# install python3
pwd
pacman -Syuu
pacman -S python3 --noconfirm
pacman -S python-pip --noconfirm
pip3 install pyyaml
# install git
pacman -S git --noconfirm
# install nodejs
pacman -S nodejs --noconfirm
pacman -S npm --noconfirm
# install nginx
pacman -S nginx --noconfirm
# install mongodb
echo "BUILDING MONGODB"
pacman -S base-devel --noconfirm
useradd -M bob
su bob -c "git clone https://aur.archlinux.org/mongodb-bin.git /tmp/mongodb-bin"
cd /tmp/mongodb-bin
su bob -c "makepkg -Acs"
pacman -U mongodb-bin* --noconfirm
rm -rf /tmp/mongodb-bin
userdel bob
cd /Auto-Encode
echo "DONE BUILDING MONGODB"
# install ffmpeg
pacman -S ffmpeg --noconfirm
# setup encode_gate
cd encode_gate
npm install -g npm-check-updates
npm-check-updates -u
npm update
npm install --no-optional
# setup encode_gui
cd ../encode_gui
npm-check-updates -u
npm update
npm update vue-template-compiler
npm install --no-optional
npm run build
mkdir -p /data/db
mkdir /srv/http/auto_encode
cp -r dist/* /srv/http/auto_encode/
# setup nginx.conf
cd ..
cp confs/nginx.conf /etc/nginx/nginx.conf
# install docker
pacman -S docker
# clean up
pacman -R python3-pip --noconfirm
pacman -R base-devel --noconfirm
pacman -R git --noconfirm
pacman -Rns $(pacman -Qtdq) --noconfirm
pacman -Scc --noconfirm
