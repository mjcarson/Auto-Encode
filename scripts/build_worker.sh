#!/bin/bash

# install python3
pacman -Syuu
pacman -S python3 --noconfirm
pacman -S python-pip --noconfirm
pip3 install pexpect
pip3 install pyyaml
# install git
pacman -S git --noconfirm
# install ffmpeg
pacman -S ffmpeg --noconfirm
#install mediainfo
pacman -S mediainfo --noconfirm
# clone repo
git clone https://gitlab.com/mjcarson/Auto-Encode.git
# install docker
pacman -S docker
# clean up
pacman -R git --noconfirm
pacman -Rns $(pacman -Qtdq) --noconfirm
pacman -Scc --noconfirm
