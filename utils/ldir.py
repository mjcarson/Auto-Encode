#!/usr/bin/env python3
'''load dir for loader to monitor'''

class Load_dir(object):
    '''load dir for loader to monitor'''
    def __init__(self, name, load_path, targ_path, share_root, presets, types, staging, container="mkv"):
        '''inits load_dir'''
        # name of load_dir profile
        self.name = name
        # path where unconverted file is on server
        self.load_path = load_path
        # path where converted file will be saved on server
        self.targ_path = targ_path
        # path on server to nfs root
        self.share_root = share_root
        # ffmpeg presets
        self.presets = presets
        # type of content converted
        self.type = types
        # path to staging on server
        self.staging_path = staging
        # containter to user for encoding
        self.container = container
