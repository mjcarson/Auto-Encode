'''Utility functions used by Auto-Encode'''

import os
import time
import base64
import shutil
import threading
import functools
import subprocess

class Utils(object):
    '''Utility Functions'''

    class PathDoesNotExist(Exception):
        '''Path does not exist'''
        pass

    class SrcDoesNotExist(Exception):
        '''Source File/Folder doesnt exist'''
        pass

    class DstDoesNotExist(Exception):
        '''Source File/Folder doesnt exist'''
        pass

    def __init__(self, config, log, capi,):
        '''inits utility functions'''
        # vars passed from caller
        self.config = config
        self.log = log
        self.capi = capi

        # log spam protection
        self.spam = []

    def _update_dl_prog(self, job_id, src, dest):
        '''
        gets/updates the download progress of a file
        
        Args:
            job_id (string): job to update with progress
            src (string): path to file being copied
            dest (string): path to where the file is being copied to
        '''
        time.sleep(5)
        # make sure both src and targ exist
        if os.path.exists(src) == False:
            self.log.error("_update_dl_prog Error: src file does not exist")
            raise self.SrcDoesNotExist
        if os.path.exists(dest) == False:
            self.log.error("_update_dl_prog Error: dest file does not exist")
            raise self.DstDoesNotExist

        # destination size
        dest_size = 0;
        # get total size of source
        total_size = os.path.getsize(src)

        while dest_size < total_size:
        # get current size of destination
            dest_size = os.path.getsize(dest)
            prog = str((dest_size / total_size) * 100).split(".")[0]
            self.capi.set_job_dl_progress(job_id, prog)
            time.sleep(5)

    def check_file(self, targ):
        '''
        check if a file is done being copied/moved

        Args:
            targ (string): path to file to check
        Returns:
            bool: true if done being moved, false if not
        '''
        # get size of targ
        size = os.stat(targ).st_size
        time.sleep(5)
        # get size of targ agin
        size_check = os.stat(targ).st_size
        # if it changed its still being written
        if size_check == size:
            return True
        return False
    
    def _create_hidden(self, path, name):
        '''
        creates hidden file

        Args:
            path: path of hidden file to create
            name: name of hidden file to create
        Returns:
            file object
        '''
        # prepend . if on *nix
        if os.name != "nt":
            name = "." + name

        # create full_path
        full_path = os.path.join(path, name)

        # write to disk
        fp  = open(full_path, "w+")
        fp.write("Auto-Encode Helper File")

        # if on windows set  hidden attribute
        if os.name == "nt":
            ret = ctypes.windll.kernel32.SetFileAttributesW(full_path, FILE_ATTRIBUTE_HIDDEN)
            if ret is False:
                raise ctypes.WinError()
        return fp
    
    def check_space(self, src, dest, sration=True):
        '''
        checks if there is enough spam on target disk for file
        creates a file descriptor called ae_helper

        Args:
            src (string): path of file to copy
            dest (string): path to place file
        Returns:
            bool: True if enough space, False if not
            sration (string, optional): follow max_disk_usage setting
        '''
        # get src size
        src_fp = open(src, "r")
        src_size = os.path.getsize(src)

        # build path to hidden file
        if os.name != "nt":
            helper_path = os.path.join(dest, "ae_helper")
        else:
            helper_path = os.path.join(dest, ".ae_helper")

        # we need a file descriptor to get available space on disk
        # make a hidden file
        # only 1 will be made at the root of the nfs share
        if os.path.exists(helper_path) == False:
                # create hidden file
                dest_fp = self._create_hidden(dest, "ae_helper")
        else:
            dest_fp = open(helper_path, "r")

        # get size on disk
        disk_stats = os.fstatvfs(dest_fp.fileno())
        disk_tsize = disk_stats.f_frsize * disk_stats.f_blocks
        disk_usize = disk_tsize - (disk_stats.f_frsize * disk_stats.f_bavail)
        # set space ration
        if sration == True:
            ratval = float(self.config["max_disk_usage"])
        else:
            ratval = 1
        if src_size + disk_usize < disk_tsize * 1:
            return True
        return False

    def copy(self, src, targ, job_id=None, sration=True):
        '''
        copies file to a target desination overwritting whatever is there
        
        Args:
            src (string): path to file to copy
            targ (string): path to place file at
            job_id (string, optional): job to update with progress
            sration (string, optional): follow max_disk_usage setting
        '''
        # make sure src file is done being written
        if self.check_file(src) == False:
            return False

        # get targ dir
        tdir = os.path.dirname(targ)
        # make targ dirs if they dont exist
        if os.path.exists(tdir) == False:
            os.makedirs(tdir)
        # delete targ if it already exists
        if os.path.exists(targ):
            os.remove(targ)

        # wait until enough space is available at target
        if self.check_space(src, tdir, sration) == False:
            if src not in self.spam:
                self.log.info("Not enough space to copy {}".format(src))
                self.spam.append(src)
            return False

        # remove from spam log
        if src in self.spam:
            self.spam.remove(src)
        
        # get/set progress of copy
        if job_id is not None:
            t = threading.Thread(target=self._update_dl_prog, args=(job_id, src, targ))
            t.start() 
        # copy  file
        self.log.info("Copying {} to tmp dir".format(src))
        shutil.copyfile(src, targ)
        self.log.info("{} copy complete".format(src))
        return True

    def cull_workers(self, workers):
        '''
        removes dead workers from workers list

        Args:
            workers ([theading.Thread]): list of thread objects
        Returns:
            workers ([theading.Thread]): list of thread objects with dead
                threads removed
        '''
        # check if workers are done
        for wname in list(workers.keys()):
            if workers[wname].isAlive() == False:
                # remove dead workers
                del workers[wname]
        return workers
 
    @staticmethod
    def _filter_empty(item, depth=1):
        '''
        Checks if a list contains anything at a specified depth 
        (zero indexed)
        Args:
            item (tuple): tuple of lists
            depth (unsigned int): nth list in tuple to check
                
        Returns:
            bool: whether list at depth contains items
        '''
        # make sure target list exists
        if len(item) >= depth:
            if len(item[depth]) > 0:
                return True
        return False
    
    def filter(self, path, depth):
        '''
        filters anything not a directory from  os.walk

        Args:
            path (string): path to return directories from
        '''
        return filter(functools.partial(self._filter_empty, \
                                        depth=depth), \
                      os.walk(path)) 

    def get_ncname(self, oname, ldir):
        '''
        gets name of episode with the new extension

        Args:
            oname (string): name of episode with old extension
        Returns:
            ncname
        '''
        # get name without extension
        nname = oname.split(".")[:-1]
        nname = ".".join(nname)

        # get name with new extension
        ncname = nname + "." + ldir.container
        return ncname
    
    def get_runtime(self, path):
        '''
        gets total time of media using ffmpeg
        
        Args:
            path (string): path to file
        Returns:
            string: runtime of media in seconds
        '''
        # get total time in movie
        cmd = "ffprobe -v error -show_entries format=duration \-of default=noprint_wrappers=1:nokey=1 \"" + path + "\""
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output = proc.stdout.read()
        total_time = output.decode("utf-8").rstrip()
        # strip the fraction of a second it just causes problems
        return total_time.split(".")[0]


    def check_dup_job(self, ldtype, path):
        '''
        checks if a movie has already been encoded
        
        Args:
            path (string): path to file
        Returns:
            bool: True if already encoded, False if not
        '''
        b64_path = base64.b64encode(path.encode()).decode("utf-8")

        # check if movie is already in encoder_gate
        if len(self.capi.get_job_path(ldtype, b64_path)) > 0:
            return True
        return False

