import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Vuetify from 'vuetify'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'

import jobstatus from './components/jobstatus'

Vue.use(Vuetify);
Vue.config.productionTip = false

Vue.component('jobstatus', jobstatus)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
