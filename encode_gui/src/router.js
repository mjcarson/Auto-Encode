import Vue from 'vue'
import Router from 'vue-router'
import home from './components/home.vue'
import shutdown from './components/shutdown.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/shutdown',
      name: 'shutdown',
      component: shutdown
    }
  ]
})
