#!/usr/bin/env bash

cp /confs/.env /encode_gui/.
cp /confs/nginx.conf /etc/nginx/nginx.conf
yarn build
mkdir -p /srv/http/auto_encode
cp -r /encode_gui/dist/* /srv/http/auto_encode/.
nginx -g "daemon off;"
