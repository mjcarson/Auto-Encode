Auto Encode

[![pipeline status](https://gitlab.com/mjcarson/Auto-Encode/badges/master/pipeline.svg)](https://gitlab.com/mjcarson/Auto-Encode/commits/docker_images)

Auto-Encode
---
Auto-Encode was built with three main goals in mind.
  - leverage a cluster to speed up encoding without loss of quality (due to distribution)  
  - extensible due to a RESTAPI  
  - require very little human interaction

### Installation
---

Using Kubernetes/Docker is recommended as it takes care of the installation of dependencies.


##### Kubernetes
---
1) Deploy Mongo either in k8s or somewhere else accesible by auto-encode to deploy in kubernetes use this stateful set deployment file in a namespace of your choosing.
```
kubectl create -f mongodb-petset.yaml -n <namespace>
kubectl create -f mongodb-service.yaml -n <namespace>
```

2) Create auto-encode namespace by deploying this file in the confs folder
```
kubectl create -f auto-encode-namespace.yaml
```

3) Fill in variables with <variable> in configs in /confs and create config maps for them in the auto-encode namespace

```
# files inside /confs/encode_gui
kubectl create configmap encode-gui --from-file=.env --from-file=nginx.conf -n auto-encode
# files inside /confs/encode_gate
kubectl create configmap encode-gate --from-file=.env -n auto-encode
# files inside /confs
kubectl create configmap ae --from-file=ae.yaml -n auto-encode
```
4) Ensure auto-encode is able to be reached through your ingress controller. Examples for traefik can be found at /k8s/*-ingress.yaml

5) Deploy Auto-Encode components
```
# all files found in /k8s
kubectl create -f encode_gate-deployment.yaml -n auto-encode
kubectl create -f encode_gate-service.yaml -n auto-encode
kubectl create -f encode_gate-ingress.yaml -n auto-encode
kubectl create -f encode_gui-deployment.yaml -n auto-encode
kubectl create -f encode_gui-service.yaml -n auto-encode
kubectl create -f encode_gui-ingress.yaml -n auto-encode
kubectl create -f loader-deployment.yaml -n auto-encode
kubectl create -f encoder-deployment.yaml -n auto-encode
```